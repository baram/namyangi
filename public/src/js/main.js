






$('.js-main-slide').slick({
    autoplay:false,
    autoplaySpeed:4000,
    slidesToShow:1,
    slidesToScroll: 1,
    dots:true,
    prevArrow:'.js-slide-prev',
	nextArrow:'.js-slide-next',
    pauseOnHover:false,
    dotsClass:'main-visual__dot',
    zIndex:20
});
