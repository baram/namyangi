$(window).on("load resize", function () {
	$('body').attr('data-mobile', function () {
		var r = $(window).width() <= 1024 ? true : false;
		return r;
	}());
	var footerH = $('.footer').outerHeight();
	$('body').css('padding-bottom',footerH);
});

$('.js-hover').on('mouseenter', function () {
  if ($('body').attr('data-mobile') == 'true') return false;
  $(this).addClass('is-hover');
}).on('mouseleave', function () {
  if ($('body').attr('data-mobile') == 'true') return false;
  $(this).removeClass('is-hover');
});

$('.js-popup-open').off().on('click',function(e){
	e.preventDefault();
	$('.popup').fadeIn(300);
});
$('.js-popup-close').off().on('click',function(e){
	e.preventDefault();
	$('.popup').fadeOut(300);
});

$('.js-terms-open').off().on('click',function(e){
	e.preventDefault();
	$('.popup__terms').fadeIn(300);
});
$('.js-terms-close').off().on('click',function(e){
	e.preventDefault();
	$('.popup__terms').fadeOut(300);
});
$('.js-gnb-btn').off().on('click',function(e){
	e.preventDefault();
	$('.gnb, .dim').addClass('is-active');
});
$('.js-gnb-close').off().on('click',function(e){
	e.preventDefault();
	$('.gnb, .dim').removeClass('is-active');
});
$('.top-btn').click(function(e){
	e.preventDefault();
	$('html,body').animate({'scrollTop':'0'},300)
});

$('.js-faq').on('click',function(e){
	e.preventDefault();
	var faqElem = $(this).closest('.faq__item');
	if(faqElem.hasClass('is-active')){
		faqElem.removeClass('is-active')
		faqElem.find('.faq__body').slideUp(500);
	}else{
		$('.faq__item').find('.faq__body').slideUp(500);
		faqElem.addClass('is-active').siblings().removeClass('is-active');
		faqElem.find('.faq__body').slideDown(500);
	}
})


$('.js-hover').on('mouseenter', function () {
  if ($('body').attr('data-mobile') == 'true') return false;
  $(this).addClass('is-hover');
}).on('mouseleave', function () {
  if ($('body').attr('data-mobile') == 'true') return false;
  $(this).removeClass('is-hover');
});

$(document).ready(function(){
	cellNoWrap($('.js-board-link'));
});

$(window).on('resize', function(e) {
	cellNormal($('.js-board-link'));
	cellNoWrap($('.js-board-link'));
}).resize();
//테이블 말줄임
function cellNoWrap(target) {
  target.css({'max-width':target.parent().width(),'white-space':'nowrap'},function(){});
}
